var mongoose = require('mongoose');

var UserSchema = mongoose.Schema({
  username: {type: String, unique: true},
  email: String,
  hash: String
});

var User = module.exports = mongoose.model('User', UserSchema);
