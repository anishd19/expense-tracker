var mongoose = require('mongoose');

var ExpenseSchema = mongoose.Schema({
  username: {type: String, unique: true},
  categoryList: [{
    name: String,
    color: String
  }],
  expenses: [{
    time: {type: Date, default: Date.now},
    title: String,
    amount: Number,
    categorySel: [{
      name: String,
      color: String
    }]
  }]
});

module.exports = mongoose.model('Expense', ExpenseSchema);
