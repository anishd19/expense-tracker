require('colors');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');

const apiController = require('./controllers/api');

const hostname = '127.0.0.1';
const port = +process.argv[2] || process.env.PORT || 3000;
const db = 'mongodb://localhost/expenseapp';

mongoose.connect(db);

const app = new express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

app.use('/api', apiController);

app.listen(port, hostname, () => {
  console.log((`Server running at http://${hostname}:${port}`).green)
});
