const express = require('express');
const router = express.Router();

const Expense = require('../models/Data');

router.get('/expense/:username', (req, res) => {
  Expense.findOne({username: req.params.username})
  .exec((err, data) => {
    if(err) {
      res.send(err);
    }else {
      console.log(data);
      res.json(data);
    }
  });
});

router.post('/expense', (req, res) => {
  Expense.findOne({
    username: req.body.username
  }).exec((err, data) => {
    if(!data) {
      Expense.create(req.body, (err, data) => {
        if(err) {
          res.send('error');
        }else {
          console.log(data);
          res.send(data);
        }
      });
    }else {
      Expense.findOneAndUpdate({
        username: req.body.username
      }, {
          $set: req.body
      }, {
          upsert: true
      }, (err, data) => {
        if(err) {
          res.send('error');
        }else {
          res.send(data)
        }
      });
    }
  });
});

router.delete('/expense', (req, res) => {
  Expense.findOneAndRemove({
    username: req.body.username
  }, (err, data) => {
    if(err) {
      res.send('error');
    }else {
      res.send(data);
    }
  });
});

module.exports = router;
